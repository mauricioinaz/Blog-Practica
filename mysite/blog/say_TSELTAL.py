NUMBERS_1_20 = [
    "jun",          #1
    "cheb",         #2
    "oxeb",         #3
    "chaneb",       #4
    "ho'eb",        #5
    "waqueb",       #6
    "huqueb",       #7
    "waxaqueb",     #8
    "baluneb",      #9
    "lajuneb",      #10
    "buhlucheb",    #11
    "lajchayeb",    #12
    "oxlajuneb",    #13
    "chanlajuneb",  #14
    "ho'lajuneb",   #15
    "waclajuneb",   #16
    "huclajuneb",   #17
    "waxaclajuneb", #18
    "balunlajuneb"  #19
    ]

MULTP_1_20 = [
    "", "j", "cha'", "ox", "chan", "ho'", "wac", "huc", "waxac", "balun",
    "lajun", "buhluch", "lajchay","oxlajun", "chanlajun", "ho'lajun",
    "waclajun", "huclajun", "waxaclajun", "balunlajun"]

PRONOUNS_1_20 = [
    "",
    "j",
    "s",
    "y",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "y",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    ]

#Other regions use jcalab insteaf of jbahc' bahqu'etic
ORDERS = ["", "winic", "bahc'", "pic", "bahc' bahqu'etic", "mam"]
#       20**0  20**1   20**2    20**3   20**4              20**5
#        0     20      400      8,000   160,000          3,200,000

B20 = 20 #Base of tseltal numbers

def say_tseltal(number):
    #limit range
    if 0 > number < 63999999 :
        raise ValueError("Name out of range / Yahtabal c'ax muc'")

    #Zero
    if number == 0:
        return "ma'yuc"

    #convert to a BASE_20 digits list
    b20_digits = [number % B20] #Obtain first digit
    while number // B20 > 0:     #Append while digits remain
        number = number // B20
        b20_digits.append(number % B20)
    print(b20_digits)

    result = ""
    prev_digits_zero = False
    for i, digit in enumerate(b20_digits):
        i, digit, prev_digits_zero, result = say_tseltal_write(i, digit, prev_digits_zero, result)

    return result

def say_tseltal_write(i, digit, prev_digits_zero, result):
    #First Digit
    if i == 0:
        if digit == 0:
            prev_digits_zero = True
        else:
            result = NUMBERS_1_20[digit - 1]
    #Rest of Digits
    else:
        if digit == 0:
            pass
        #Special CASE for 20
        elif i == 1 and prev_digits_zero and digit == 1:
            result += "jtahb"
            prev_digits_zero = False
        #Exception for 19s
        elif digit == 19 and not(prev_digits_zero):
            result += " s" + ORDERS[i + 1] + "al"
        #Whole digits
        elif prev_digits_zero:
            prev_digits_zero = False
            result += MULTP_1_20[digit] + ORDERS[i]
        #General Scenario
        else:
            result += " " + PRONOUNS_1_20[digit + 1] + MULTP_1_20[digit + 1] + ORDERS[i]
    return i, digit, prev_digits_zero, result
