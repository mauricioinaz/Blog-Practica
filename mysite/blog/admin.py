from django.contrib import admin
from .models import Post, Comment, Tag


class PostAdmin(admin.ModelAdmin):
    #list display to modify list view
    list_display = ('title', 'published_date', 'created_date', 'author', 'visible')
    #display manytomany in two columns
    filter_horizontal = ('tags',)
    #date filer
    list_filter = ['published_date']
    #search option
    search_fields = ['text']

admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Tag)
