from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post, Tag
from django.db.models import Q
from django.http import HttpResponse
from blog.say_TSELTAL import say_tseltal
from .forms import CommentForm

def post_list(request):
    FRONTPAGE_AUTHOR = 2
    FRONTPAGE_MAX_VIEWS = 2
    posts = Post.objects.filter(author=FRONTPAGE_AUTHOR, visible=True).order_by('-published_date')[:FRONTPAGE_MAX_VIEWS]

    return render(request, 'blog/post_list_az.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    #include try...
    tags = post.tags.all()
    comments = post.comments.all()
    return render(request, 'blog/post_detail_az.html',
        {
        'post': post,
        'tags': tags,
        'comments': comments
        })

def tseltal_number(request):
    error = False
    if request.GET.get('q', None):
        try:
            number_tseltal = say_tseltal(int(request.GET.get('q')))
        except:
            error = True
            number_tseltal = ''
        return render(request, 'blog/tseltal_number.html', {
        'number_tseltal': number_tseltal, 'number': request.GET.get('q'), 'error': error
        })
    return render(request, 'blog/tseltal_number.html', {'error': True})

def search(request, ):
    errors = []
    SEARCH_LIMIT = 5
    if request.GET.get('q', None):
        q = request.GET['q']
        posts = Post.objects.filter(Q(title__icontains=q) | Q(text__icontains=q) | Q(tags__tag_name=q))[:SEARCH_LIMIT]
        return render(request, 'blog/search_results.html', {'posts': posts, 'query': q})
    errors.append('You entered a blank search.')
    return render(request, 'blog/search_results.html', {'errors': errors})


def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'blog/add_comment_to_post.html', {'form': form})
