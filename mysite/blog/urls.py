from django.conf.urls import url
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.post_list, name='post_list'),
    url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
    url(r'^post/(?P<pk>\d+)/comment/$', views.add_comment_to_post, name='add_comment_to_post'),
    url(r'^tseltal_number/$', views.tseltal_number, name='tseltal_number'),
    url(r'^search/$', views.search, name='search'),

]
