from django.contrib import admin

from .models import Choice, Question

#al principio solo era:
#admin.site.register(Question)

#para despliege vertical
#class ChoiceInline(admin.StackedInline):
#para despliegue tabulado
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

#cambiamos el orden de pregunta y fecha.
#luego lo ponemos en tuplas.
class QuestionAdmin(admin.ModelAdmin):
    #fields = ['pub_date', 'question_text']
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    #esto agregamos para meter de una vez los 'choices'
    inlines = [ChoiceInline]
    #list display es el que te da el poder para cómos e visualisa la listaself.
    #es una lista administrable
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    #para agregar un filtro por fecha:
    list_filter = ['pub_date']
    #para poder buscar:
    search_fields = ['question_text']

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)
